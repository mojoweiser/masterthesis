# libraries
import tweepy
import pymongo
import sqlite3
import collections
import datetime
import sys

# check version
print("Tweepy-Version:", tweepy.__version__)

# sqlite3 connection
db_main = "main.sqlite"
db_config = "config.sqlite"

# pull search_id from cron-job
def fetch_id():
    # Use a breakpoint in the code line below to debug your script.
    # if id from cron is missing get id 1
    if len(sys.argv)>1:
        searchQuery = "SELECT direct_user_id, username, max_results_per_page, pages, excludes, col_name, since_id, until_id, start_time, end_time FROM suchauftraege where id = {}".format(sys.argv[1])
        print(f'Search Query: {searchQuery}')
    else:
        print(f'Suchauftrag-ID angeben!')
        searchQuery = "SELECT direct_user_id, username, max_results_per_page, pages, excludes, col_name, since_id, until_id, start_time, end_time FROM suchauftraege where id = 1"

    return searchQuery


# pull variables from suchauftraege table
def search_variables():
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: main.sqlite
    :return: Connection object or None
    """
    # build connection
    conn = None
    try:
        conn = sqlite3.connect(db_main)
    except Exception as e:
        print(e)

    # cursor for querying
    cur = conn.cursor()

    #SQL string execute
    searchQuery = fetch_id()
    cur.execute(searchQuery)

    # fetch & safe parameters in variables
    variables = cur.fetchall()

    # close sqlite connection
    cur.close()

    # iterate through variables for the parameters
    for variable in variables:
        direct_user_id = variable[0]
        username = variable[1]
        max_results_per_page = variable[2]
        pages = variable[3]
        excludes = variable[4]
        col_name = variable[5]
        since_id = variable[6]
        until_id = variable[7]
        start_time = variable[8]
        end_time = variable[9]
    return direct_user_id, username, max_results_per_page, pages, excludes, col_name, since_id, until_id, start_time, end_time

direct_user_id, username, max_results_per_page, pages, excludes, col_name, since_id, until_id, start_time, end_time = search_variables()

# set excludes to None if empty
if excludes is not None and len(excludes) < 1:
    excludes = None
print("Ausgeschlossene Tweets:", excludes)

# pull variables from config table for twitter api authentication
def api_sqlite():
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: main.sqlite
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_config)
    except Exception as e:
        print(e)

    cur = conn.cursor()
    cur.execute("""SELECT bearer_token, consumer_key, consumer_key_secret, access_token, access_token_secret 
                   FROM main.config WHERE id=1
                   """)

    rows = cur.fetchall()
    cur.close()

    for row in rows:
        bearer_token = row[0]
        consumer_key = row[1]
        consumer_key_secret = row[2]
        access_token = row[3]
        access_token_secret = row[4]
        client = tweepy.Client(bearer_token=bearer_token, consumer_key=consumer_key,
                               consumer_secret=consumer_key_secret, access_token=access_token,
                               access_token_secret=access_token_secret, wait_on_rate_limit=True)
    return client

# build connection to DokumentDB-Cluster
def mongo_sqlite():
    conn = None
    try:
        conn = sqlite3.connect(db_config)
    except Exception as e:
        print(e)

    cur = conn.cursor()
    cur.execute("SELECT client_db FROM config")

    mongodb = cur.fetchall()
    cur.close()

    for row in mongodb:
        client_db = row[0]
        config = pymongo.MongoClient(host=client_db)
    return config


# pull, transform and send tweets
class tweets_timeline:
    # mongodb connection
    config = mongo_sqlite()
    # to write to/access db: config.mongo_db.XYZ
    mongodb = config.Twitter
    # create/access collection: db.XYZ
    collection = mongodb[col_name]
    # indexing
    collection.create_index([("id", pymongo.ASCENDING)], unique=True)

    # get user-id from username
    if direct_user_id is None:
        def get_user_id(username):
            client = api_sqlite()
            results = []
            if direct_user_id is None:
                users = client.get_users(usernames=username)
                for user in users.data:
                    obj = {"id": user["id"]}
                    results.append(obj)
            return results

    # get user_id
    user_data = get_user_id(username)

    # user_id from username or from sqlite database
    try:
        user_id = user_data[0]["id"]
    except:
        user_id = direct_user_id

    # data collection from users-endpoint
    def searchTweets(user_id):
        client = api_sqlite()
        # raw list with dictionaries
        tweets = []
        # pagination -> iteration contains includes and meta
        try:
            #paginate the search - searching for users timeline
            for tweet in tweepy.Paginator(client.get_users_tweets,
                                           id=user_id,  # if ID is known, than direct_user_id
                                           since_id=since_id,  # returns results with a Tweet ID greater than
                                           until_id=until_id,
                                           start_time=start_time,  # YYYY-MM-DDTHH:mm:ssZ (ISO 8601/RFC 3339)
                                           end_time=end_time,
                                           # exclude=["retweets"] / exclude=["replies"] / exclude=[""]
                                           exclude=excludes,
                                           # request all tweet data
                                           tweet_fields=["attachments", "author_id", "context_annotations",
                                                         "conversation_id",
                                                         "created_at", "entities", "geo", "in_reply_to_user_id", "lang",
                                                         "public_metrics", "referenced_tweets", "reply_settings", "source"
                                                         ],
                                           place_fields=["contained_within", "country", "country_code", "full_name",
                                                         "geo", "id", "name", "place_type"
                                                         ],
                                           expansions=["attachments.poll_ids", "attachments.media_keys", "author_id",
                                                       "entities.mentions.username", "geo.place_id", "in_reply_to_user_id",
                                                       "referenced_tweets.id", "referenced_tweets.id.author_id"
                                                       ],
                                           user_fields=["created_at", "description", "entities", "location", "name",
                                                        "pinned_tweet_id", "profile_image_url", "protected",
                                                        "public_metrics", "url", "username", "verified", "withheld"
                                                        ],
                                           poll_fields=["duration_minutes", "end_datetime", "id", "options", "voting_status"
                                                        ],
                                           media_fields=["duration_ms", "height", "media_key", "preview_image_url", "type",
                                                         "url", "width", "public_metrics", "non_public_metrics", "organic_metrics",
                                                         "promoted_metrics", "alt_text"],
                                           max_results=max_results_per_page,            # tweets per page
                                           limit=pages,                                 # pages to retrieve
                                           ):
                tweets.append(tweet)

        except Exception as error:
            print(error)
            error_string = str(error)
            first_word = error_string.split()[0]
            int_code = int(first_word)
            now = datetime.datetime.now()
            run = "Timeline"

            conn = None
            try:
                conn = sqlite3.connect(db_main)
            except Exception as e:
                print(e)

            cur = conn.cursor()

            # write Error into log table
            cur.execute("INSERT INTO log VALUES (?,?,?,?,?,?)", (username, run, int_code, error_string, now, ""))

            conn.commit()
            print("Timeline-Error siehe log")
            cur.close()

        return tweets

    # save the respond from the API in tweet_data
    tweet_data = searchTweets(user_id)

    # list and dictionaries with data
    result = []
    user_dict = {}
    tweet_dict = {}
    place_dict = {}
    poll_dict = {}
    media_dict = {}


    if not tweet_data is None and len(tweet_data) > 0:  # iteration - if None or len = 0 then empty string

        # docs https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet
        # safe attachment data in dictionaries with their unique identifier
        for response in tweet_data:
            if 'users' in response.includes:
                # Take all of the users, and put them into a dictionary
                for user in response.includes['users']:
                    user_dict[user.id] = user

            if 'tweets' in response.includes:
                # Take all of the tweets, and put them into a dictionary
                for tweet in response.includes['tweets']:
                    tweet_dict[tweet.id] = tweet

            if 'places' in response.includes:
                # Take all of the places, and put them into a dictionary
                for place in response.includes['places']:
                    place_dict[place["id"]] = place

            if 'polls' in response.includes:
                # Take all of the polls, and put them into a dictionary
                for poll in response.includes['polls']:
                    poll_dict[poll["id"]] = poll

            if 'media' in response.includes:
                # Take all of the media, and put them into a dictionary
                for media in response.includes['media']:
                    media_dict[media.media_key] = media

            # connect dictionaries with tweets with their unique identifier
            if response.data is not None:
                for tweet in response.data:

                    # For each tweet, find the author's information
                    author_info = user_dict[tweet["author_id"]]

                    # For each tweet, find their original tweet information (retweets, quoted)
                    if tweet["referenced_tweets"] is not None:
                        # https://github.com/DocNow/twarc-csv/blob/main/dataframe_converter.py#L301
                        # change the structure of the data object
                        referenced_tweets = [
                            {"type": r["type"], "ref_id": r["id"]} for r in tweet["referenced_tweets"]
                        ]
                        # ChainMap flattens list into properties
                        tweet_ref = dict(collections.ChainMap(*referenced_tweets))
                    else:
                        tweet_ref = {"type": "original"}

                    try:
                        # bind the referenced tweet to the tweet from the timeline
                        tweet_info = tweet_dict[tweet_ref["ref_id"]]
                    except:
                        tweet_info = None


                    if tweet["context_annotations"] is not None:
                        # https://stackoverflow.com/questions/35864007/python-3-5-iterate-through-a-list-of-dictionaries
                        context_annotations = [
                            {"Annotation_{}".format(index): {"domain": r["domain"], "entity": r["entity"]}} for index, r in
                            enumerate(tweet["context_annotations"])
                        ]
                        # ChainMap flattens list into properties
                        tweet_con = dict(collections.ChainMap(*context_annotations))
                    else:
                        tweet_con = tweet["context_annotations"]


                    # for each tweet find the attachment
                    if tweet["attachments"] is not None:
                        try:
                            attachments = [
                                {"media_keys": r} for r in tweet.attachments["media_keys"]
                            ]
                            # ChainMap flattens list into properties
                            attachment = dict(collections.ChainMap(*attachments))
                            media_info = media_dict[attachment["media_keys"]]
                            poll_info = None
                        except:
                            attachments = [
                                {"poll_ids": r} for r in tweet.attachments["poll_ids"]
                            ]
                            # ChainMap flattens list into properties
                            attachment = dict(collections.ChainMap(*attachments))

                            poll_info = poll_dict[attachment["poll_ids"]]
                            media_info = None
                    else:
                        attachment = None
                        media_info = None
                        poll_info = None

                    if tweet.geo is not None:
                        place_info = place_dict[tweet.geo["place_id"]]
                    else:
                        place_info = None

                    #safe the data in a json-format
                    result.append(
                          {"id": tweet.id,
                           "author_id": tweet.author_id,
                           "user": author_info,
                           "text": tweet.text,
                           "conversation_id": tweet.conversation_id,
                           "in_reply_to_user_id": tweet.in_reply_to_user_id,
                           "created_at": tweet.created_at,
                           "referenced_tweet": tweet_info,
                           "created_at": tweet.created_at,
                           "public_metrics": tweet.public_metrics,
                           "in_reply_to_user_id": tweet.in_reply_to_user_id,
                           "referenced_tweets": tweet_ref,
                           "attachments": attachment,
                           "entities": tweet.entities,
                           "context_annotations": tweet_con,
                           "poll_fields": poll_info,
                           "media_fields": media_info,
                           "geo": tweet.geo,
                           "place_fields": place_info,
                           "lang": tweet.lang,
                           "public_metrics": tweet.public_metrics,
                           "reply_settings": tweet.reply_settings,
                           "source": tweet.source,
                           "withheld": tweet.withheld,
                           "url": "https://twitter.com/{}/status/{}".format(username, tweet.id),
                           }
                    )


    # insert/update MongoDB collection
    for x in result:
        try:
            collection.insert_one(x)
        except:  # if id is identical mongodb will throw error (unique = True) -> update
            collection.update_many({"id": x["id"]}, {"$set": {"public_metrics":
                                                                  {"retweet_count": x["public_metrics"][
                                                                      "retweet_count"],
                                                                   "reply_count": x["public_metrics"][
                                                                       "reply_count"],
                                                                   "like_count": x["public_metrics"]["like_count"],
                                                                   "quote_count": x["public_metrics"][
                                                                       "quote_count"]}},
                                                     "$currentDate": {"lastModified": True}})

    # sqlite3 - safe session params
    tweet_send = len(result)
    now = datetime.datetime.now()
    run = "Timeline"

    conn = None
    try:
        conn = sqlite3.connect(db_main)
    except Exception as e:
        print(e)

    cur = conn.cursor()

    #send the session parameter into log table
    cur.execute("INSERT INTO log VALUES (?,?,?,?,?,?)", (username, run, "", "", now, tweet_send))
    conn.commit()
    print("Tweets commited")
    cur.close()

def main():
    tweets_timeline()


if __name__ == '__main__':
    main()