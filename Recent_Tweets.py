# libraries
import tweepy
import pymongo
import sqlite3
import collections
import datetime
import sys

# check version
print("Tweepy-Version:", tweepy.__version__)

# sqlite3 connection
db_main = "main.sqlite"
db_config = "config.sqlite"


# pull search_id from cron-job with specific variables
def fetch_id():
    # Use a breakpoint in the code line below to debug your script.
    if len(sys.argv) > 1:
        searchQuery = """SELECT query, max_results_per_page, pages, col_name, since_id, until_id, start_time, end_time FROM suchauftraege 
                         WHERE id = {}""".format(sys.argv[1])
        print(f'Search Query: {searchQuery}')
    else:
        print(f'Suchauftrag-ID angeben!')
        searchQuery = "SELECT query, max_results_per_page, pages, col_name, since_id, until_id, start_time, end_time FROM suchauftraege where id = 3"
    return searchQuery


# pull variables from suchauftraege table
def search_variables():
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: main.sqlite
    :return: Connection object or None
    """
    # build connection
    conn = None
    try:
        conn = sqlite3.connect(db_main)
    except Exception as e:
        print(e)

    # cursor for querying
    cur = conn.cursor()

    # SQL query execute
    searchQuery = fetch_id()
    cur.execute(searchQuery)

    # fetch & safe parameters in variables
    variables = cur.fetchall()

    # close sqlite connection
    cur.close()

    # iterate through variables for the parameters
    for variable in variables:
        query = variable[0]
        max_results_per_page = variable[1]
        pages = variable[2]
        col_name = variable[3]
        since_id = variable[4]
        until_id = variable[5]
        start_time = variable[6]
        end_time = variable[7]
    return query, max_results_per_page, pages, col_name, since_id, until_id, start_time, end_time


query, max_results_per_page, pages, col_name, since_id, until_id, start_time, end_time = search_variables()

# pull variables from config table for twitter api authentication
def api_sqlite():
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: main
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_config)
    except Exception as e:
        print(e)

    cur = conn.cursor()

    cur.execute("""SELECT bearer_token, consumer_key, consumer_key_secret, access_token, access_token_secret 
                   FROM config 
                   """)

    rows = cur.fetchall()
    cur.close()

    for row in rows:
        bearer_token = row[0]
        consumer_key = row[1]
        consumer_key_secret = row[2]
        access_token = row[3]
        access_token_secret = row[4]
        client = tweepy.Client(bearer_token=bearer_token, consumer_key=consumer_key,
                               consumer_secret=consumer_key_secret, access_token=access_token,
                               access_token_secret=access_token_secret, wait_on_rate_limit=True)
    return client


# build connection to DokumentDB-Cluster
def mongo_sqlite():
    conn = None
    try:
        conn = sqlite3.connect(db_config)
    except Exception as e:
        print(e)

    cur = conn.cursor()

    cur.execute("SELECT client_db FROM config")

    mongodb = cur.fetchall()
    cur.close()

    for row in mongodb:
        client_db = row[0]
        config = pymongo.MongoClient(host=client_db)
    return config


# search Query - https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query
# pull, transform and send tweets
class recent_tweets:
    # mongodb connection
    config = mongo_sqlite()
    # to write to/access db: config.mongo_db.XYZ
    mongodb = config.Twitter
    # create/access collection: db.XYZ
    collection = mongodb[col_name]
    # indexing
    collection.create_index([("id", pymongo.ASCENDING)], unique=True)

    # data collection from recent-endpoint
    def searchTweets(query):
        client = api_sqlite()
        # raw list with dictionaries
        tweets = []
        # pagination
        try:
            for tweet in tweepy.Paginator(client.search_recent_tweets,
                                          # only tweets from the last 7days! - no tweets no results
                                          query=query,
                                          since_id=since_id,  # returns results with a Tweet ID greater than (newer tweets higher id)
                                          until_id=until_id,
                                          start_time=start_time,  # YYYY-MM-DDTHH:mm:ssZ (ISO 8601/RFC 3339)
                                          end_time=end_time,
                                          # docs: https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet
                                          tweet_fields=["id", "public_metrics", "conversation_id", "created_at", "geo",
                                                        "lang",
                                                        "in_reply_to_user_id", "referenced_tweets", "reply_settings",
                                                        "source", "text", "withheld", "entities"
                                                        ],
                                          expansions=["attachments.poll_ids", "attachments.media_keys", "author_id",
                                                      "entities.mentions.username", "geo.place_id",
                                                      "in_reply_to_user_id",
                                                      "referenced_tweets.id", "referenced_tweets.id.author_id"
                                                      ],
                                          media_fields=["duration_ms", "height", "media_key", "preview_image_url",
                                                        "type",
                                                        "url", "width", "public_metrics", "alt_text"
                                                        ],
                                          place_fields=["contained_within", "country", "country_code", "full_name",
                                                        "geo", "id", "name", "place_type"
                                                        ],
                                          poll_fields=["duration_minutes", "end_datetime", "id", "options",
                                                       "voting_status"
                                                       ],
                                          user_fields=["created_at", "description", "entities", "id", "location",
                                                       "name", "pinned_tweet_id", "profile_image_url", "protected",
                                                       "public_metrics", "url", "username", "verified", "withheld"
                                                       ],
                                          # possible to receive less than the max_results per request throughout the pagination process - 10 results minimum
                                          max_results=max_results_per_page,
                                          limit=pages  # number of iterations: max_results * limit = pulls
                                          ):
                tweets.append(tweet)

        except Exception as error:
            print(error)
            error_string = str(error)
            first_word = error_string.split()[0]
            int_code = int(first_word)
            now = datetime.datetime.now()
            run = "Recent_Tweets"

            conn = None
            try:
                conn = sqlite3.connect(db_main)
            except Exception as e:
                print(e)

            cur = conn.cursor()

            # write Error into log table
            cur.execute("INSERT INTO log VALUES (?,?,?,?,?,?)", (query, run, int_code, error_string, now, ""))

            conn.commit()
            print("Recent_Tweet-Error siehe log")
            cur.close()

        return tweets

    # save the respond from the API in tweets_query
    tweets_query = searchTweets(query)  # build a query: https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query

    # list and dictionaries with data
    result = []
    tweet_dict = {}
    user_dict = {}
    place_dict = {}
    poll_dict = {}
    media_dict = {}

    if not tweets_query is None and len(tweets_query) > 0:  # iteration - if None or len = 0 then empty string

        # safe attachment data in dictionaries with their unique identifier in dictionaries
        for response in tweets_query:
            if 'tweets' in response.includes:
                for data in response.includes['tweets']:
                    tweet_dict[data.id] = data

            if 'users' in response.includes:
                # Take all of the users, and put them into a dictionary
                for user in response.includes['users']:
                    user_dict[user.id] = user

            if 'places' in response.includes:
                for place in response.includes['places']:
                    place_dict[place["id"]] = place

            if 'polls' in response.includes:
                for poll in response.includes['polls']:
                    poll_dict[poll["id"]] = poll

            if 'media' in response.includes:
                for media in response.includes['media']:
                    media_dict[media.media_key] = media

            # connect dictionaries with tweets with their unique identifier
            for tweet in response.data:
                # For each tweet, find the author's information
                author_info = user_dict[tweet.author_id]

                # For each tweet, find their original tweet information (retweets, quoted)
                if tweet["referenced_tweets"] is not None:
                    # https://github.com/DocNow/twarc-csv/blob/main/dataframe_converter.py#L301
                    referenced_tweets = [
                        {"type": r["type"], "ref_id": r["id"]} for r in tweet["referenced_tweets"]
                    ]
                    # leave behind references, but not the full tweets
                    # ChainMap flattens list into properties
                    tweet_ref = dict(collections.ChainMap(*referenced_tweets))
                else:
                    tweet_ref = {"type": "original"}

                if tweet_ref["type"] is "original":
                    original_tweet = None
                else:
                    try:
                        original_tweet = tweet_dict[tweet_ref["ref_id"]]
                    except:
                        original_tweet = "error occured"

                # for each tweet find the attachment
                if tweet["attachments"] is not None:
                    try:
                        attachments = [
                            {"media_keys": r} for r in tweet.attachments["media_keys"]
                        ]
                        # leave behind references, but not the full tweets
                        # ChainMap flattens list into properties
                        attachment = dict(collections.ChainMap(*attachments))

                        media_info = media_dict[attachment["media_keys"]]
                        poll_info = None
                    except:
                        attachments = [
                            {"poll_ids": r} for r in tweet.attachments["poll_ids"]
                        ]
                        # leave behind references, but not the full tweets
                        # ChainMap flattens list into properties
                        attachment = dict(collections.ChainMap(*attachments))

                        poll_info = poll_dict[attachment["poll_ids"]]
                        media_info = None
                else:
                    attachment = None
                    media_info = None
                    poll_info = None

                if tweet.geo is not None:
                    place_info = place_dict[tweet.geo["place_id"]]
                else:
                    place_info = None

                # Put all of the information we want to keep in a single dictionary for each tweet
                result.append(
                    {"id": tweet.id,
                     "conversation_id": tweet.conversation_id,
                     "author_id": tweet.author_id,
                     "user": author_info,
                     "text": tweet.text,
                     "original_tweet": original_tweet,
                     "created_at": tweet.created_at,
                     "referenced": tweet_ref,
                     "entities": tweet.entities,
                     "attachments": attachment,
                     "in_reply_to_user_id": tweet.in_reply_to_user_id,
                     "reply_settings": tweet.reply_settings,
                     "public_metrics": {"retweet_count": tweet.public_metrics["retweet_count"],
                                        "reply_count": tweet.public_metrics["reply_count"],
                                        "like_count": tweet.public_metrics["like_count"],
                                        "quote_count": tweet.public_metrics["quote_count"]
                                        },
                     "source": tweet.source,
                     "lang": tweet.lang,
                     "withheld": tweet.withheld,
                     "poll_fields": poll_info,
                     "media_fields": media_info,
                     "geo": tweet.geo,
                     "place_fields": place_info,
                     "url": "https://twitter.com/{}/status/{}".format(author_info['username'], tweet["id"])
                     }
                )

    # insert/update MongoDB collection
    for x in result:
        try:
            # mongodb
            collection.insert_one(x)  # insert in MongoDB
        except:
            collection.update_many({"id": x["id"]}, {"$set": {"public_metrics":
                                                                  {"retweet_count": x["public_metrics"][
                                                                      "retweet_count"],
                                                                   "reply_count": x["public_metrics"]["reply_count"],
                                                                   "like_count": x["public_metrics"]["like_count"],
                                                                   "quote_count": x["public_metrics"]["quote_count"]}},
                                                     "$currentDate": {"lastModified": True}})

    # sqlite3 - save session params
    tweet_send = len(result)
    now = datetime.datetime.now()
    run = "Recent_Tweets"
    conn = None
    try:
        conn = sqlite3.connect(db_main)
    except Exception as e:
        print(e)

    cur = conn.cursor()

    cur.execute("INSERT INTO log VALUES (?,?,?,?,?,?)", (query, run, "", "", now, tweet_send))
    conn.commit()
    print("Tweets commited")
    cur.close()


def main():
    recent_tweets()


if __name__ == '__main__':
    main()
